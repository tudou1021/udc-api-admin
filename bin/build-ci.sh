#!/bin/bash

export
CURRENT_FILE_DIR="$(cd "$(dirname "$0")";pwd)"
cd $CURRENT_FILE_DIR
cd ../
set -e
export PATH=/apollo/env/maven/thirdparty.maven/bin/:$PATH
export PATH=/apollo/env/jdk7/thirdparty.jdk/bin/:$PATH
export PATH=/apollo/env/ApolloBuildClient/apollo.ApolloBuildClient/bin/:$PATH
mvn -version
function build_ci() {
    if [ $# == 1 ] ; then
       command=$1
    else
        echo "command: build,test,sonar,publish"
        exit 1
    fi
    if [[ $command = 'build' ]]; then
        mvn clean package -U -Dmaven.test.skip=true
    elif [[ $command = 'test' ]]; then
        #mvn clean package -U
       echo "ignore test"
    elif [[ $command = 'sonar' ]]; then
        mvn sonar:sonar
    elif [[ $command = 'publish_jar' ]]; then
        mvn deploy
    elif [[ $command = 'publish_package' ]]; then
        mvn clean package -U -Dmaven.test.skip=true
        mkdir udc-api-admin && cp -r bin udc-api-admin && unzip target/*.jar -d target/temp && cp target/*.jar udc-api-admin
        cd udc-api-admin && chmod 755 * && publish_package udc-api-admin
    else
        echo 'unrecognized command $command'
    fi
}
build_ci $1

