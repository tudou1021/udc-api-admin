package com.vdian.udc.apiadmin.mapper.uds;

import com.vdian.udc.apiadmin.entity.uds.ApiInterface;
import com.vdian.udc.apiadmin.querycase.uds.ApiInterfaceQueryCase;

import java.util.List;

public interface ApiInterfaceMapper {
    int deleteByPrimaryKey(Integer interfaceId);

    int insert(ApiInterface record);

    int insertSelective(ApiInterface record);

    ApiInterface selectByPrimaryKey(Integer interfaceId);

    int updateByPrimaryKeySelective(ApiInterface record);

    int updateByPrimaryKeyWithBLOBs(ApiInterface record);

    int updateByPrimaryKey(ApiInterface record);

    List<ApiInterface> getApiIntefaceList(ApiInterfaceQueryCase queryCase);

    Long getApiIntefaceCount(ApiInterfaceQueryCase queryCase);
}