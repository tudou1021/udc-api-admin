package com.vdian.udc.apiadmin.mapper.uds;

import com.vdian.udc.apiadmin.entity.uds.ApiInterfaceCategory;
import com.vdian.udc.apiadmin.querycase.uds.ApiInterfaceCategoryQueryCase;
import sun.rmi.runtime.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface ApiInterfaceCategoryMapper {
    int deleteByPrimaryKey(Integer interfaceCategoryId);

    int insert(ApiInterfaceCategory record);

    int insertSelective(ApiInterfaceCategory record);

    ApiInterfaceCategory selectByPrimaryKey(Integer interfaceCategoryId);

    int updateByPrimaryKeySelective(ApiInterfaceCategory record);

    int updateByPrimaryKey(ApiInterfaceCategory record);

    List<ApiInterfaceCategory> getApiInterfaceCategoryList(ApiInterfaceCategoryQueryCase queryCase);

    long getApiInterfaceCategoryTotalCount(ApiInterfaceCategoryQueryCase queryCase);

}