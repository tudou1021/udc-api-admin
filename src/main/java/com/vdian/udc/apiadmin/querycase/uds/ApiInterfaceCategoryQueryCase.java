package com.vdian.udc.apiadmin.querycase.uds;

import com.vdian.udc.apiadmin.entity.Pager;

import java.util.Set;

public class ApiInterfaceCategoryQueryCase {

    String searchPhrase;

    Integer isValid;

    Pager pager;

    Set<Integer> categroyIdSet;

    public String getSearchPhrase() {
        return searchPhrase;
    }

    public void setSearchPhrase(String searchPhrase) {
        this.searchPhrase = searchPhrase;
    }

    public Pager getPager() {
        return pager;
    }

    public void setPager(Pager pager) {
        this.pager = pager;
    }

    public Integer getIsValid() {
        return isValid;
    }

    public void setIsValid(Integer isValid) {
        this.isValid = isValid;
    }

    public Set<Integer> getCategroyIdSet() {
        return categroyIdSet;
    }

    public void setCategroyIdSet(Set<Integer> categroyIdSet) {
        this.categroyIdSet = categroyIdSet;
    }


}
