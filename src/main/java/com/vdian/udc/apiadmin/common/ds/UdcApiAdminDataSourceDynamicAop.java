package com.vdian.udc.apiadmin.common.ds;

import com.vdian.udc.apiadmin.common.logger.LoggerFactory;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

/**
 * @Title:数据源动态拦截器
 * @Description:根据ServiceName匹配方法名称设置数据源
 * @author:xu.he
 * @create:2016/12/15 上午9:59
 * @version:v1.0
 */
@Component
@Aspect
public class UdcApiAdminDataSourceDynamicAop {

    private Logger logger= LoggerFactory.getLogger(UdcApiAdminDataSourceDynamicAop.class);

    @Before("execution(* com.vdian.udc.apiadmin.mapper.*.*.query*(..)) " +
            "|| execution(* com.vdian.udc.apiadmin.consumer.mapper.*.*.get*(..)) " +
            "|| execution(* com.vdian.udc.apiadmin.consumer.mapper.*.*.select*(..)) " +
            "|| execution(* com.vdian.udc.apiadmin.consumer.mapper.*.*.load*(..)) " +
            "|| execution(* com.vdian.udc.apiadmin.consumer.mapper.*.*.count*(..)) " +
            "|| execution(* com.vdian.udc.apiadmin.consumer.mapper.*.*.check*(..)) " +
            "|| execution(* com.vdian.udc.apiadmin.consumer.mapper.*.*.list*(..)) " )
    public void setReadDataSource(){
        UdcApiAdminDataSourceContextHolder.read();
    }

    @Before("execution(* com.vdian.udc.apiadmin.consumer.mapper.*.*.update*(..)) " +
            "|| execution(* com.vdian.udc.apiadmin.consumer.mapper.*.*.insert*(..)) " +
            "|| execution(* com.vdian.udc.apiadmin.consumer.mapper.*.*.add*(..)) " +
            "|| execution(* com.vdian.udc.apiadmin.consumer.mapper.*.*.save*(..)) " +
            "|| execution(* com.vdian.udc.apiadmin.consumer.mapper.*.*.create*(..)) " +
            "|| execution(* com.vdian.udc.apiadmin.consumer.mapper.*.*.edit*(..)) " +
            "|| execution(* com.vdian.udc.apiadmin.consumer.mapper.*.*.remove*(..)) " +
            "|| execution(* com.vdian.udc.apiadmin.consumer.mapper.*.*.delete*(..))")
    public void setWriteDataSource(){
        UdcApiAdminDataSourceContextHolder.write();
    }

}
