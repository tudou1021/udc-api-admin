package com.vdian.udc.apiadmin.common.config;

import com.vdian.udc.apiadmin.common.ds.UdcApiAdminDataSource;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Title:Mybatis配置
 * @Description:TODO
 * @author:xu.he
 * @create:2017-02-15 11:41
 * @version:v1.0
 */
@Configuration
@AutoConfigureAfter(UdcApiAdminDataSource.class)
public class UdcApiAdminMybatisConfig {

    @Bean
    public MapperScannerConfigurer udsMapperScannerConfigurer() {
        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
        mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
        mapperScannerConfigurer.setBasePackage("com.vdian.udc.apiadmin.mapper");
        return mapperScannerConfigurer;
    }

}
