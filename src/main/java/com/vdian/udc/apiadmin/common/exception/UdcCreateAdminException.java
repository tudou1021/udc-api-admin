package com.vdian.udc.apiadmin.common.exception;


import com.vdian.udc.context.exception.UdcException;

/**
 * @Title:异常类
 * @Description:TODO
 * @author:xu.he
 * @create:2017-02-15 11:14
 * @version:v1.0
 */
public class UdcCreateAdminException extends UdcException {

    public UdcCreateAdminException(int code, String reason) {
        super(code, reason);
    }
}
