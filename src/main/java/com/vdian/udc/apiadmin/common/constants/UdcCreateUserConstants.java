package com.vdian.udc.apiadmin.common.constants;

/**
 * @Title:常量类
 * @Description:TODO
 * @author:xu.he
 * @create:2017-01-16 14:17
 * @version:v1.0
 */
public class UdcCreateUserConstants {

    public static final String UDC_CONSUMER_LOGOFF_TOPIC="AccountChange";

    public static final String UDC_CONSUMER_LOGOFF_TAGS="logoff";
}
