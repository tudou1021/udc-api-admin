package com.vdian.udc.apiadmin.common.ds;

/**
 * @Title:数据源类型
 * @Description:TODO
 * @author:xu.he
 * @create:2016/12/15 上午10:45
 * @version:v1.0
 */
public enum UdcApiAdminDataSourceType {
    READ,//读
    WRITE//写
}
