package com.vdian.udc.apiadmin.common.logger;

import org.slf4j.Logger;

public class LoggerFactory {

    static final Logger apiAccessLogger = org.slf4j.LoggerFactory.getLogger("apiAccessLogger");

    static final Logger serviceAccessLogger = org.slf4j.LoggerFactory.getLogger("serviceAccessLogger");

    static final Logger apiErrorLogger = org.slf4j.LoggerFactory.getLogger("apiErrorLogger");

    static final Logger serviceErrorLogger = org.slf4j.LoggerFactory.getLogger("serviceErrorLogger");

    public static Logger getApiAccessLogger() {
        return apiAccessLogger;
    }

    public static Logger getApiErrorLogger() {
        return apiErrorLogger;
    }

    public static Logger getServiceAccessLogger() {
        return serviceAccessLogger;
    }

    public static Logger getServiceErrorLogger() {
        return serviceErrorLogger;
    }

    public static <T> Logger getLogger(Class<T> clazz) {
        return org.slf4j.LoggerFactory.getLogger(clazz);
    }
}
