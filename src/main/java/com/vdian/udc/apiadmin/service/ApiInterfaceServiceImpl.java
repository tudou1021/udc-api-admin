package com.vdian.udc.apiadmin.service;

import com.github.pagehelper.PageHelper;
import com.vdian.udc.apiadmin.entity.uds.ApiInterface;
import com.vdian.udc.apiadmin.entity.Pager;
import com.vdian.udc.apiadmin.entity.uds.ApiInterfaceCategory;
import com.vdian.udc.apiadmin.mapper.uds.ApiInterfaceCategoryMapper;
import com.vdian.udc.apiadmin.mapper.uds.ApiInterfaceMapper;
import com.vdian.udc.apiadmin.querycase.uds.ApiInterfaceCategoryQueryCase;
import com.vdian.udc.apiadmin.querycase.uds.ApiInterfaceQueryCase;
import com.vdian.udc.common.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service("apiInterfaceService")
public class ApiInterfaceServiceImpl implements ApiInterfaceService {

    @Autowired
    ApiInterfaceMapper apiInterfaceMapper;

    @Autowired
    ApiInterfaceCategoryMapper apiInterfaceCategoryMapper;

    @Override
    public List<ApiInterface> getApiInterfaceList(ApiInterfaceQueryCase queryCase) {
        if(queryCase!=null&&queryCase.getPager()!=null){
            Pager pager = queryCase.getPager();
            PageHelper.startPage(pager.getCurrentPage(), pager.getPageSize());
        }
        List<ApiInterface> apiInterfaceList=apiInterfaceMapper.getApiIntefaceList(queryCase);
        if(CollectionUtils.isEmpty(apiInterfaceList)){
            return Collections.EMPTY_LIST;
        }

        Set<Integer> categoryIdSet = new HashSet<>();
        for (ApiInterface apiInterface : apiInterfaceList) {
            categoryIdSet.add(apiInterface.getInterfaceCategory());
        }

        if(!CollectionUtils.isEmpty(categoryIdSet)){
            ApiInterfaceCategoryQueryCase categoryQueryCase = new ApiInterfaceCategoryQueryCase();
            categoryQueryCase.setCategroyIdSet(categoryIdSet);
            List<ApiInterfaceCategory> categoryList = apiInterfaceCategoryMapper.getApiInterfaceCategoryList(categoryQueryCase);
            Map<Integer, ApiInterfaceCategory> categoryMap = convertCategoryListToMap(categoryList);
            if(!org.springframework.util.CollectionUtils.isEmpty(categoryMap)){
                for (ApiInterface apiInterface : apiInterfaceList) {
                    ApiInterfaceCategory category = categoryMap.get(apiInterface.getInterfaceCategory());
                    apiInterface.setCategory(category);
                }
            }
        }

        return apiInterfaceList;
    }

    @Override
    public long getApiInterfaceCount(ApiInterfaceQueryCase queryCase) {
        return apiInterfaceMapper.getApiIntefaceCount(queryCase);
    }

    @Override
    public ApiInterface getApiInterfaceById(Integer interfaceId) {
        return apiInterfaceMapper.selectByPrimaryKey(interfaceId);
    }

    @Override
    public Integer addApiInterface(ApiInterface apiInterface) {
        return apiInterfaceMapper.insertSelective(apiInterface);
    }

    @Override
    public Integer updateApiInterface(ApiInterface apiInterface) {
        return apiInterfaceMapper.updateByPrimaryKeySelective(apiInterface);
    }

    @Override
    public Integer deleteApiInterfaceById(Integer interfaceId) {
        return apiInterfaceMapper.deleteByPrimaryKey(interfaceId);
    }

    private Map<Integer,ApiInterfaceCategory> convertCategoryListToMap(List<ApiInterfaceCategory> categoryList){
        if(CollectionUtils.isEmpty(categoryList)){
            return Collections.EMPTY_MAP;
        }
        Map<Integer,ApiInterfaceCategory> retMap = new HashMap<>();
        for (ApiInterfaceCategory category : categoryList) {
            retMap.put(category.getInterfaceCategoryId(),category);
        }
        return retMap;
    }

}
