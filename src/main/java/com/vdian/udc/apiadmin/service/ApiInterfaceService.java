package com.vdian.udc.apiadmin.service;

import com.vdian.udc.apiadmin.entity.uds.ApiInterface;
import com.vdian.udc.apiadmin.querycase.uds.ApiInterfaceQueryCase;

import java.util.List;

/**
 * @Title:udc-api-admin
 * @Description:
 * @author:xu.he
 * @version:v1.0
 */
public interface ApiInterfaceService {

    List<ApiInterface> getApiInterfaceList(ApiInterfaceQueryCase queryCase);

    long getApiInterfaceCount(ApiInterfaceQueryCase queryCase);

    ApiInterface getApiInterfaceById(Integer interfaceId);

    Integer addApiInterface(ApiInterface apiInterface);

    Integer updateApiInterface(ApiInterface apiInterface);

    Integer deleteApiInterfaceById(Integer interfaceId);

}
