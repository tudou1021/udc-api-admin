package com.vdian.udc.apiadmin.service;

import com.vdian.udc.apiadmin.entity.uds.ApiInterfaceCategory;
import com.vdian.udc.apiadmin.querycase.uds.ApiInterfaceCategoryQueryCase;

import java.util.List;

public interface ApiInterfaceCategoryService {

    List<ApiInterfaceCategory> getApiInterfaceCategoryList(ApiInterfaceCategoryQueryCase queryCase);

    Long getApiInterfaceCategoryTotalCount(ApiInterfaceCategoryQueryCase queryCase);

    ApiInterfaceCategory getApiInterfaceCategoryById(Integer categoryId);

    Integer updateApiInterfaceCategory(ApiInterfaceCategory apiInterfaceCategory);

    Integer addApiInterfaceCategory(ApiInterfaceCategory apiInterfaceCategory);

    Integer deleteApiInterfaceCategoryById(Integer categoryId);
}
