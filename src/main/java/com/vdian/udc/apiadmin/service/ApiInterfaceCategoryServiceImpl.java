package com.vdian.udc.apiadmin.service;

import com.github.pagehelper.PageHelper;
import com.vdian.udc.apiadmin.entity.uds.ApiInterfaceCategory;
import com.vdian.udc.apiadmin.entity.Pager;
import com.vdian.udc.apiadmin.mapper.uds.ApiInterfaceCategoryMapper;
import com.vdian.udc.apiadmin.querycase.uds.ApiInterfaceCategoryQueryCase;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service("apiInterfaceCategoryService")
public class ApiInterfaceCategoryServiceImpl implements ApiInterfaceCategoryService {

    @Autowired
    ApiInterfaceCategoryMapper apiInterfaceCategoryMapper;

    @Override
    public List<ApiInterfaceCategory> getApiInterfaceCategoryList(ApiInterfaceCategoryQueryCase queryCase) {
        if(queryCase!=null&&queryCase.getPager()!=null){
            Pager pager = queryCase.getPager();
            PageHelper.startPage(pager.getCurrentPage(), pager.getPageSize());
        }
        List<ApiInterfaceCategory> categoryList=apiInterfaceCategoryMapper.getApiInterfaceCategoryList(queryCase);
        if(CollectionUtils.isEmpty(categoryList)){
            return Collections.EMPTY_LIST;
        }
        return categoryList;
    }

    @Override
    public Long getApiInterfaceCategoryTotalCount(ApiInterfaceCategoryQueryCase queryCase) {
        return apiInterfaceCategoryMapper.getApiInterfaceCategoryTotalCount(queryCase);
    }

    @Override
    public ApiInterfaceCategory getApiInterfaceCategoryById(Integer categoryId) {
        return apiInterfaceCategoryMapper.selectByPrimaryKey(categoryId);
    }

    @Override
    public Integer updateApiInterfaceCategory(ApiInterfaceCategory apiInterfaceCategory) {
        return apiInterfaceCategoryMapper.updateByPrimaryKeySelective(apiInterfaceCategory);
    }

    @Override
    public Integer addApiInterfaceCategory(ApiInterfaceCategory apiInterfaceCategory) {
        return apiInterfaceCategoryMapper.insertSelective(apiInterfaceCategory);
    }

    @Override
    public Integer deleteApiInterfaceCategoryById(Integer categoryId) {
        return apiInterfaceCategoryMapper.deleteByPrimaryKey(categoryId);
    }
}
