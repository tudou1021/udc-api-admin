//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.vdian.udc.apiadmin.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class Pager implements Serializable {
    private int totalRows = 0;
    private int pageSize = 10;
    private int currentPage = 1;
    private int totalPages = 1;
    private int startRow = 0;
    private String linkUrl;
    private List listResult;
    private Map<String, Object> mapResult;

    public List getListResult() {
        return this.listResult;
    }

    public void setListResult(List listResult) {
        this.listResult = listResult;
    }

    public Pager() {
    }

    public void operatePager() {
        this.totalPages = this.totalRows / this.pageSize;
        int mod = this.totalRows % this.pageSize;
        if(mod > 0) {
            ++this.totalPages;
        }

    }

    public Pager(int _totalRows, int _pageSize) {
        this.totalRows = _totalRows;
        this.pageSize = _pageSize;
        this.totalPages = this.totalRows / this.pageSize;
        int mod = this.totalRows % this.pageSize;
        if(mod > 0) {
            ++this.totalPages;
        }

        this.currentPage = 1;
        this.startRow = 0;
    }

    public Pager(int currentPage, int _totalRows, int _pageSize) {
        int totalPages1 = _totalRows / _pageSize;
        int mod1 = _totalRows % _pageSize;
        this.totalRows = _totalRows;
        this.pageSize = _pageSize;
        if(mod1 > 0) {
            ++totalPages1;
        }

        if(currentPage > totalPages1) {
            --currentPage;
        }

        if(currentPage == 0) {
            this.setStart(1);
        } else {
            this.setStart(currentPage);
        }

        this.totalPages = this.totalRows / this.pageSize;
        int mod = this.totalRows % this.pageSize;
        if(mod > 0) {
            ++this.totalPages;
        } else if(currentPage == 0) {
            boolean var7 = true;
        }

    }

    public void setStart(int currentPage) {
        this.currentPage = currentPage;
        this.startRow = (currentPage - 1) * this.pageSize;
    }

    public int getTotalRows() {
        return this.totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getCurrentPage() {
        if(this.currentPage == 0) {
            this.currentPage = 1;
        }

        return this.currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPages() {
        return this.totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getStartRow() {
        return this.startRow;
    }

    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    public String getLinkUrl() {
        return this.linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public Map<String, Object> getMapResult() {
        return this.mapResult;
    }

    public void setMapResult(Map<String, Object> mapResult) {
        this.mapResult = mapResult;
    }
}