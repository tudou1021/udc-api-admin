package com.vdian.udc.apiadmin.entity.uds;

import java.util.Date;

public class ApiInterfaceCategory {

    private Integer interfaceCategoryId;

    private String categoryName;

    private Byte isValid;

    private Date createTime;

    private String categoryDesc;

    public Integer getInterfaceCategoryId() {
        return interfaceCategoryId;
    }

    public void setInterfaceCategoryId(Integer interfaceCategoryId) {
        this.interfaceCategoryId = interfaceCategoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName == null ? null : categoryName.trim();
    }

    public Byte getIsValid() {
        return isValid;
    }

    public void setIsValid(Byte isValid) {
        this.isValid = isValid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCategoryDesc() {
        return categoryDesc;
    }

    public void setCategoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc == null ? null : categoryDesc.trim();
    }
}