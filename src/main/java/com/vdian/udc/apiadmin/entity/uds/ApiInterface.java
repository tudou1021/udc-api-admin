package com.vdian.udc.apiadmin.entity.uds;

import java.util.Date;

public class ApiInterface {
    private Integer interfaceId;

    private String interfaceName;

    private Integer interfaceCategory;

    private String interfaceUrl;

    private Byte isValid;

    private Date createTime;

    private String interfaceDesc;

    private String interfaceParam;

    private ApiInterfaceCategory category;

    public Integer getInterfaceId() {
        return interfaceId;
    }

    public void setInterfaceId(Integer interfaceId) {
        this.interfaceId = interfaceId;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName == null ? null : interfaceName.trim();
    }

    public Integer getInterfaceCategory() {
        return interfaceCategory;
    }

    public void setInterfaceCategory(Integer interfaceCategory) {
        this.interfaceCategory = interfaceCategory;
    }

    public String getInterfaceUrl() {
        return interfaceUrl;
    }

    public void setInterfaceUrl(String interfaceUrl) {
        this.interfaceUrl = interfaceUrl == null ? null : interfaceUrl.trim();
    }

    public Byte getIsValid() {
        return isValid;
    }

    public void setIsValid(Byte isValid) {
        this.isValid = isValid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getInterfaceDesc() {
        return interfaceDesc;
    }

    public void setInterfaceDesc(String interfaceDesc) {
        this.interfaceDesc = interfaceDesc == null ? null : interfaceDesc.trim();
    }

    public String getInterfaceParam() {
        return interfaceParam;
    }

    public void setInterfaceParam(String interfaceParam) {
        this.interfaceParam = interfaceParam == null ? null : interfaceParam.trim();
    }

    public ApiInterfaceCategory getCategory() {
        return category;
    }

    public void setCategory(ApiInterfaceCategory category) {
        this.category = category;
    }
}