package com.vdian.udc.apiadmin.entity;

import java.io.Serializable;
import java.util.List;

/**
 * @Title:列表对象
 * @Description:
 * @author:xu.he
 * @create:2017-03-17 16:34
 * @version:v1.0
 */
public class BootGridVo<T> implements Serializable{

    private static final long serialVersionUID = -6209821150294386258L;

    private int current;
    private int rowCount;
    private int total;
    private List<T> rows;

    public BootGridVo(int current, int rowCount, int total, List<T> rows) {
        this.current = current;
        this.rowCount = rowCount;
        this.total = total;
        this.rows = rows;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public int getRowCount() {
        return rowCount;
    }

    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }

}
