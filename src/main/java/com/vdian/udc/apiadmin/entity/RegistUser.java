package com.vdian.udc.apiadmin.entity;

import com.vdian.udc.common.util.JsonUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @Title:udc-api-admin
 * @Description:
 * @author:xu.he
 * @create:2017-03-20 18:22
 * @version:v1.0
 */
public class RegistUser {

    /**
     * userName : 用户1
     * countryCode : 86
     * telephone : 18600198020
     */

    private String userName;
    private String countryCode;
    private String telephone;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public static void main(String[] args) {
        List<RegistUser> userList = new ArrayList<>();
        for (int i = 0; i < 10 ; i++) {
            RegistUser user = new RegistUser();
            user.setUserName("测试用户"+i);
            user.setCountryCode("86");
            user.setTelephone("1860019801"+i);
            userList.add(user);
        }
        System.out.println(JsonUtils.toJson(userList));
    }
}
