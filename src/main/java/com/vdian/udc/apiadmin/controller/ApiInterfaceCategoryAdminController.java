package com.vdian.udc.apiadmin.controller;

import com.vdian.udc.apiadmin.entity.BootGridVo;
import com.vdian.udc.apiadmin.entity.Pager;
import com.vdian.udc.apiadmin.entity.uds.ApiInterfaceCategory;
import com.vdian.udc.apiadmin.querycase.uds.ApiInterfaceCategoryQueryCase;
import com.vdian.udc.context.api.UdcResponse;
import com.vdian.udc.context.exception.UdcException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/category")
public class ApiInterfaceCategoryAdminController extends BaseController{

    @RequestMapping("/toCategoryIndex")
    public String toCategoryIndex(){
        return "category/list_category";
    }

    @RequestMapping("/toAddCategroy")
    public String toAddCategroy(){
        return "category/add_category";
    }

    @RequestMapping("/toModifyCategory")
    public String toModifyCategory(@RequestParam("categoryId")Integer categoryId, HttpServletRequest request){
        if(null == categoryId){
            return "500";
        }
        ApiInterfaceCategory category = apiInterfaceCategoryService.getApiInterfaceCategoryById(categoryId);
        request.setAttribute("category",category);
        return "category/modify_category";
    }


    @RequestMapping("/queryCategoryList")
    public @ResponseBody Object queryCategoryList(@RequestParam("current") Integer current,
                                                  @RequestParam("rowCount") Integer rowCount,
                                                  @RequestParam("searchPhrase") String searchPhrase){
        if(current == null ||rowCount == null){
            return "500";
        }
        Pager pager = new Pager();
        pager.setCurrentPage(current);
        pager.setPageSize(rowCount);
        ApiInterfaceCategoryQueryCase queryCase = new ApiInterfaceCategoryQueryCase();
        queryCase.setSearchPhrase(searchPhrase);
        queryCase.setPager(pager);
        Long totalCount = apiInterfaceCategoryService.getApiInterfaceCategoryTotalCount(queryCase);
        List<ApiInterfaceCategory> categoryList = new ArrayList<>();
        if(totalCount != 0){
             categoryList = apiInterfaceCategoryService.getApiInterfaceCategoryList(queryCase);
        }
        return new BootGridVo(current, rowCount, totalCount.intValue(), categoryList);
    }

    @RequestMapping("/saveCategory")
    public @ResponseBody UdcResponse saveCategory(@RequestParam("categoryId")Integer categoryId,
                                                  @RequestParam("method")String method,
                                                  @RequestParam("categoryName")String categoryName,
                                                  @RequestParam("isValid")Integer isValid,
                                                  @RequestParam("categoryDesc")String categoryDesc){
        if(StringUtils.isBlank(method)||StringUtils.isBlank(categoryName)||null == isValid){
            return new UdcResponse(UdcException.PARAMETER_ERROR);
        }
        ApiInterfaceCategory category = new ApiInterfaceCategory();
        category.setCategoryDesc(categoryDesc);
        category.setCategoryName(categoryName);
        category.setIsValid(isValid.byteValue());
        try {
            if(method.equals("save")){
                category.setCreateTime(new Date());
                apiInterfaceCategoryService.addApiInterfaceCategory(category);
            }else {
                if(null == categoryId){
                    return new UdcResponse(UdcException.PARAMETER_ERROR);
                }
                category.setInterfaceCategoryId(categoryId);
                apiInterfaceCategoryService.updateApiInterfaceCategory(category);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new UdcResponse(UdcException.UNKNOWN);
        }
        return new UdcResponse();
    }

    @RequestMapping("/deleteCategory")
    public @ResponseBody UdcResponse deleteCategory(@RequestParam("categoryId")Integer categoryId){
        if(null == categoryId){
            return new UdcResponse(UdcException.PARAMETER_ERROR);
        }
        apiInterfaceCategoryService.deleteApiInterfaceCategoryById(categoryId);
        return new UdcResponse();
    }

}
