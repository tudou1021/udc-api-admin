package com.vdian.udc.apiadmin.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.vdian.udc.apiadmin.entity.uds.ApiInterface;
import com.vdian.udc.apiadmin.entity.uds.ApiInterfaceCategory;
import com.vdian.udc.apiadmin.querycase.uds.ApiInterfaceCategoryQueryCase;
import com.vdian.udc.apiadmin.querycase.uds.ApiInterfaceQueryCase;
import com.vdian.udc.common.util.HttpClientUtils;
import com.vdian.udc.common.util.JsonUtils;
import com.vdian.udc.common.util.StringUtils;
import com.vdian.udc.context.api.UdcResponse;
import com.vdian.udc.context.exception.UdcException;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @Title:请求处理接口
 * @Description:
 * @author:xu.he
 * @create:2017-03-20 14:59
 * @version:v1.0
 */
@Controller
@RequestMapping("/api")
public class ApiInterfaceController extends BaseController {

    @RequestMapping("/toApiIndex")
    public String toApiIndex(HttpServletRequest request){
        ApiInterfaceCategoryQueryCase queryCase = new ApiInterfaceCategoryQueryCase();
        List<ApiInterfaceCategory> categoryList = apiInterfaceCategoryService.getApiInterfaceCategoryList(queryCase);
        request.setAttribute("categoryList",categoryList);
        return "/api/api_index";
    }

    @RequestMapping("/queryInterfaceByCategoryId")
    public @ResponseBody UdcResponse queryInterfaceByCategoryId(@RequestParam("categoryId")Integer categoryId){
        if(null == categoryId){
            return new UdcResponse(UdcException.PARAMETER_ERROR);
        }
        ApiInterfaceQueryCase queryCase = new ApiInterfaceQueryCase();
        queryCase.setCategoryId(categoryId);
        queryCase.setIsValid(1);
        List<ApiInterface> interfaceList = apiInterfaceService.getApiInterfaceList(queryCase);
        return new UdcResponse(interfaceList);
    }

    @RequestMapping("/queryInterfaceById")
    public @ResponseBody UdcResponse queryInterfaceById(@RequestParam("interfaceId")Integer interfaceId){
        if(null == interfaceId){
            return new UdcResponse(UdcException.PARAMETER_ERROR);
        }
        ApiInterface apiInterface = apiInterfaceService.getApiInterfaceById(interfaceId);
        return new UdcResponse(apiInterface);
    }

    @RequestMapping("/createApiRequestUrl")
    public @ResponseBody UdcResponse createApiRequestUrl(@RequestParam("host")String host,
                                                         @RequestParam("interfaceId")Integer interfaceId,
                                                         @RequestParam("requestParamJson")String requestParamJson){
        if(StringUtils.isBlank(host)||null == interfaceId || StringUtils.isBlank(requestParamJson)){
            return new UdcResponse(UdcException.PARAMETER_ERROR);
        }
        ApiInterface apiInterface = apiInterfaceService.getApiInterfaceById(interfaceId);
        String requestUrl = new StringBuilder(host).append(apiInterface.getInterfaceUrl()).toString();
        List<Map> objList = JsonUtils.toList(requestParamJson,Map.class);
        String paramString = null;
        if(!CollectionUtils.isEmpty(objList)){
            Map<String,String> retMap = new HashMap<>();
            for (Map<String,Object> param : objList) {
                String key = (String) param.get("paramName");
                String value = (String) param.get("paramValue");
                retMap.put(key,value);
            }
            paramString=JsonUtils.toJson(retMap);
        }
        Map<String,String> retMap = new HashMap<>();
        retMap.put("requestUrl",requestUrl);
        retMap.put("paramString",paramString);
        return new UdcResponse(retMap);
    }

    @RequestMapping("/sendRequest")
    public @ResponseBody UdcResponse sendRequest(@RequestParam("requestUrl")String requestUrl,@RequestParam("requestParam")String requestParam){
        if(StringUtils.isBlank(requestParam) || StringUtils.isBlank(requestParam)){
            return new UdcResponse(UdcException.PARAMETER_ERROR);
        }
        Map<String,String> paramMap = JsonUtils.toMap(requestParam,String.class,String.class);
        String responseTxt = HttpClientUtils.doPost(requestUrl,paramMap);
        return new UdcResponse(responseTxt);
    }

}
