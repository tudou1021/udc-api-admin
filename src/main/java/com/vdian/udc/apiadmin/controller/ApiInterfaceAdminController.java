package com.vdian.udc.apiadmin.controller;

import com.vdian.udc.apiadmin.entity.BootGridVo;
import com.vdian.udc.apiadmin.entity.Pager;
import com.vdian.udc.apiadmin.entity.uds.ApiInterface;
import com.vdian.udc.apiadmin.entity.uds.ApiInterfaceCategory;
import com.vdian.udc.apiadmin.querycase.uds.ApiInterfaceCategoryQueryCase;
import com.vdian.udc.apiadmin.querycase.uds.ApiInterfaceQueryCase;
import com.vdian.udc.context.api.UdcResponse;
import com.vdian.udc.context.exception.UdcException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/interface")
public class ApiInterfaceAdminController extends BaseController{

    @RequestMapping("/toInterfaceIndex")
    public String toInterfaceIndex(HttpServletRequest request){
        ApiInterfaceCategoryQueryCase queryCase = new ApiInterfaceCategoryQueryCase();
        List<ApiInterfaceCategory> categoryList = apiInterfaceCategoryService.getApiInterfaceCategoryList(queryCase);
        request.setAttribute("categoryList",categoryList);
        return "interface/list_interface";
    }

    @RequestMapping("/queryInterfaceList")
    public @ResponseBody Object queryInterfaceList(@RequestParam("current") Integer current,
                                     @RequestParam("rowCount") Integer rowCount,
                                     @RequestParam("categoryId") Integer categoryId,
                                     @RequestParam("isValid") Integer isValid,
                                     @RequestParam("searchPhrase") String searchPhrase){
        if(current == null ||rowCount == null){
            return "500";
        }
        Pager pager = new Pager();
        pager.setCurrentPage(current);
        pager.setPageSize(rowCount);
        ApiInterfaceQueryCase queryCase = new ApiInterfaceQueryCase();
        queryCase.setPager(pager);
        queryCase.setInterfaceName(searchPhrase);
        queryCase.setCategoryId(categoryId);
        queryCase.setIsValid(isValid);
        Long totalCount = apiInterfaceService.getApiInterfaceCount(queryCase);
        List<ApiInterface> apiIntefaceList = new ArrayList<>();
        if(totalCount != 0){
            apiIntefaceList = apiInterfaceService.getApiInterfaceList(queryCase);
        }
        return new BootGridVo(current, rowCount, totalCount.intValue(), apiIntefaceList);
    }

    @RequestMapping("/toAddInterface")
    public String toAddInterface(HttpServletRequest request){
        ApiInterfaceCategoryQueryCase queryCase = new ApiInterfaceCategoryQueryCase();
        List<ApiInterfaceCategory> categoryList = apiInterfaceCategoryService.getApiInterfaceCategoryList(queryCase);
        request.setAttribute("categoryList",categoryList);
        return "interface/add_interface";
    }

    @RequestMapping("/toModifyInterface")
    public String toModifyInterface(@RequestParam("interfaceId")Integer interfaceId, HttpServletRequest request){
        if(null == interfaceId){
            return "500";
        }
        ApiInterfaceCategoryQueryCase queryCase = new ApiInterfaceCategoryQueryCase();
        List<ApiInterfaceCategory> categoryList = apiInterfaceCategoryService.getApiInterfaceCategoryList(queryCase);
        ApiInterface apiInterface = apiInterfaceService.getApiInterfaceById(interfaceId);
        request.setAttribute("categoryList",categoryList);
        request.setAttribute("interface",apiInterface);
        return "interface/modify_interface";
    }


    @RequestMapping("/saveInterface")
    public @ResponseBody UdcResponse saveInterface(ApiInterface apiInterface,@RequestParam("method")String method){
        if(null == apiInterface || null == apiInterface.getInterfaceCategory() || StringUtils.isBlank(apiInterface.getInterfaceName())
                || StringUtils.isBlank(apiInterface.getInterfaceUrl())|| null == apiInterface.getIsValid() || StringUtils .isBlank(apiInterface.getInterfaceParam())){
            return new UdcResponse(UdcException.PARAMETER_ERROR);
        }

        if(method.equals("save")){
            apiInterface.setCreateTime(new Date());
            apiInterfaceService.addApiInterface(apiInterface);
        }else {
            if(null == apiInterface.getInterfaceId()){
                return new UdcResponse(UdcException.PARAMETER_ERROR);
            }
            apiInterfaceService.updateApiInterface(apiInterface);
        }
        return new UdcResponse();
    }

    @RequestMapping("/deleteInterface")
    public @ResponseBody UdcResponse deleteInterface(@RequestParam("interfaceId")Integer interfaceId){
        if(null == interfaceId){
            return new UdcResponse(UdcException.PARAMETER_ERROR);
        }
        apiInterfaceService.deleteApiInterfaceById(interfaceId);
        return new UdcResponse();
    }

}
