package com.vdian.udc.apiadmin.controller;

import com.vdian.udc.common.util.JsonUtils;
import com.vdian.udc.context.api.UdcResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @Title:系统测试
 * @Description:
 * @author:xu.he
 * @create:2017-03-21 18:57
 * @version:v1.0
 */
@Controller
@RequestMapping("/test")
public class TestController {
    @RequestMapping("/requestApi")
    public @ResponseBody UdcResponse requestApi(@RequestParam("param") String param){
        Map<String,String> paramMap = JsonUtils.toMap(param,String.class,String.class);
        return new UdcResponse(paramMap);
    }

}
