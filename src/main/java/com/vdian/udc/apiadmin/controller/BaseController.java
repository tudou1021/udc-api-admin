package com.vdian.udc.apiadmin.controller;

import com.vdian.udc.apiadmin.service.ApiInterfaceCategoryService;
import com.vdian.udc.apiadmin.service.ApiInterfaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class BaseController {

    @Autowired
    protected ApiInterfaceCategoryService apiInterfaceCategoryService;

    @Autowired
    protected ApiInterfaceService apiInterfaceService;



}
